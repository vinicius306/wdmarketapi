'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URLs and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')
//venda
Route.resource('/venda', 'VendaController').apiOnly()

// Users
Route.resource('/usuario', 'UsuarioController').apiOnly()
Route.post('/login', 'UsuarioController.login')

//Produtos
//Route.resource('/produto', 'ProdutoController').apiOnly()
Route.get('/produto/:id', 'ProdutoController.index')
//ProdutosCategoria
Route.resource('/produtoCategoria', 'ProdutoCategoriaController').apiOnly().only(['index'])
//ProdutosCategoria
Route.resource('/lojaProduto', 'LojaProdutoController').apiOnly().only(['index'])
//ProdutosCategoria
Route.get('/loja/:cod', 'LojaController.index')
Route.post('/loja', 'LojaController.confirm')
//Categoria
Route.resource('/categoria', 'CategoriaController').apiOnly().only(['index'])
Route.get('/updateDb/:id', 'UpdateDbController.index')
Route.get('/', ({ response }) => {
	response.send("API WdMarkets");
})

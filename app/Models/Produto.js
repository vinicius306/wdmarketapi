'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Produto extends Model {
	static boot() {
		super.boot()

	}
	static get table() {
		return 'produto';
	}
	static get primaryKey() {
		return 'cod_produto';
	}
	getNome(nome) {
		return nome
	}
	produtocategoria() {
		return this.hasMany('App/Models/ProdutoCategoria', 'cod_produto', 'cod_produto')
	}
	categorias() {
		return this.manyThrough('App/Models/ProdutoCategoria', 'categorias', 'cod_produto', 'cod_produto')
	}
	lojas() {
		return this.manyThrough('App/Models/LojaProduto', 'lojas', 'cod_produto', 'cod_produto')
	}
	static scopeHasLojas(query) {
		return query.has('lojas')
	}

}



module.exports = Produto

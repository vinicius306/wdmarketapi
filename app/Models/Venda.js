'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Venda extends Model {
	static get table() {
		return 'venda';
	}
	static get primaryKey() {
		return 'cod_venda';
	}
	static get dates() {
		return super.dates.concat(['created', 'modified'])
	}
	static get createdAtColumn() {
		return 'created';
	}
	static get updatedAtColumn() {
		return 'modified';
	}
	vendaproduto() {
		return this.hasMany('App/Models/VendaProduto', 'cod_venda', 'cod_venda')
	}
	produtos() {
		return this.belongsToMany('App/Models/Produto', 'cod_venda', 'cod_produto', 'cod_venda', 'cod_produto')
			.pivotModel('App/Models/VendaProduto')
	}
}

module.exports = Venda

'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')
const Hash = use('Hash')

class Usuario extends Model {
	static boot() {
		super.boot()

		/**
		 * A hook to hash the user password before saving
		 * it to the database.
		 */
		this.addHook('beforeSave', async (userInstance) => {
			if (userInstance.dirty.senha) {
				userInstance.senha = await Hash.make(userInstance.senha)
			}
		})
	}
	static get createdAtColumn() {
		return 'created';
	}

	static get updatedAtColumn() {
		return 'modified';
	}
	static get dates() {
		return super.dates.concat(['created', 'modified'])
	}
	static get table() {
		return 'usuario';
	}
	static get primaryKey() {
		return 'cod_usuario';
	}
}

module.exports = Usuario

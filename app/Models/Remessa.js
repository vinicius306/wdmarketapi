'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Remessa extends Model {
	static get table() {
		return 'remessa';
	}
	static get primaryKey() {
		return 'cod_remessa';
	}
}

module.exports = Remessa

'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Transacao extends Model {
	static get table() {
		return 'transacao';
	}
	static get primaryKey() {
		return 'cod_transacao';
	}
	static get createdAtColumn() {
		return 'created';
	}
	static get updatedAtColumn() {
		return 'modified';
	}
}

module.exports = Transacao

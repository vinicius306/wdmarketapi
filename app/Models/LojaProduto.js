'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class LojaProduto extends Model {
	static get table() {
		return 'loja_produto';
	}
	lojas() {
		return this.hasOne('App/Models/Loja', 'cod_loja', 'cod_loja')
	}
}

module.exports = LojaProduto

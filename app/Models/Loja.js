'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Loja extends Model {
	static get table() {
		return 'loja';
	}
	static get primaryKey() {
		return 'cod_loja';
	}
	static get createdAtColumn() {
		return 'created';
	}
	static get updatedAtColumn() {
		return 'modified';
	}
}

module.exports = Loja

'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class ProdutoCategoria extends Model {
	static get table() {
		return 'produto_categoria';
	}
	categorias() {
		return this.hasOne('App/Models/Categoria', 'cod_categoria', 'cod_categoria')
	}
}

module.exports = ProdutoCategoria

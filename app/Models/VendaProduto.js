'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class VendaProduto extends Model {
	static get table() {
		return 'venda_produto';
	}
	static get primaryKey() {
		return 'cod_vend_prod';
	}
	static get createdAtColumn() {
		return 'created';
	}
	static get updatedAtColumn() {
		return 'modified';
	}
	// produtos() {
	// 	return this.hasOne('App/Models/Produto', 'cod_produto', 'cod_produto')
	// }
}

module.exports = VendaProduto

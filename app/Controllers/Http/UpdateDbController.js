'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */
const ProdutoCategoria = use("App/Models/ProdutoCategoria")
const { ReturnApi } = require('../../../lib/ReturnApi')
const Produto = use("App/Models/Produto")
const Categoria = use("App/Models/Categoria")
const Loja = use('App/Models/Loja')
const LojaProduto = use('App/Models/LojaProduto')
const Database = use('Database')
/**
 * Resourceful controller for interacting with updatedbs
 */
class UpdateDbController {
	/**
	 * Show a list of all updatedbs.
	 * GET updatedbs
	 *
	 * @param {object} ctx
	 * @param {Request} ctx.request
	 * @param {Response} ctx.response
	 * @param {View} ctx.view
	 */
	async index({ request, response, params }) {
		const res = {};
		const loja = await Loja.findBy('codigo', params.id)
		const Produtos = await Produto
			.query()
			.whereHas('lojas', (builder) => {
				builder.where('loja.codigo', Database.raw(`?`, [`${params.id}`]))
			})
			//.with('produtocategoria')
			// .innerJoin('loja_produto', function () {
			// 	this
			// 		.on('produto.cod_produto', 'loja_produto.cod_produto')
			// 		.andOn('loja_produto.cod_loja',)
			// })
			.fetch()
		const pc = await ProdutoCategoria.all();
		const Categorias = await Categoria.all();
		const lojaproduto = await LojaProduto.query().where('cod_loja', loja.cod_loja).fetch()
		res.loja = loja;
		res.produtos = Produtos;
		res.produto_categoria = pc;
		res.categorias = Categorias;
		res.loja_produto = lojaproduto;
		return ReturnApi(res, true)



	}

	/**
	 * Render a form to be used for creating a new updatedb.
	 * GET updatedbs/create
	 *
	 * @param {object} ctx
	 * @param {Request} ctx.request
	 * @param {Response} ctx.response
	 * @param {View} ctx.view
	 */
	async create({ request, response, view }) {
	}

	/**
	 * Create/save a new updatedb.
	 * POST updatedbs
	 *
	 * @param {object} ctx
	 * @param {Request} ctx.request
	 * @param {Response} ctx.response
	 */
	async store({ request, response }) {
	}

	/**
	 * Display a single updatedb.
	 * GET updatedbs/:id
	 *
	 * @param {object} ctx
	 * @param {Request} ctx.request
	 * @param {Response} ctx.response
	 * @param {View} ctx.view
	 */
	async show({ params, request, response, view }) {
	}

	/**
	 * Render a form to update an existing updatedb.
	 * GET updatedbs/:id/edit
	 *
	 * @param {object} ctx
	 * @param {Request} ctx.request
	 * @param {Response} ctx.response
	 * @param {View} ctx.view
	 */
	async edit({ params, request, response, view }) {
	}

	/**
	 * Update updatedb details.
	 * PUT or PATCH updatedbs/:id
	 *
	 * @param {object} ctx
	 * @param {Request} ctx.request
	 * @param {Response} ctx.response
	 */
	async update({ params, request, response }) {
	}

	/**
	 * Delete a updatedb with id.
	 * DELETE updatedbs/:id
	 *
	 * @param {object} ctx
	 * @param {Request} ctx.request
	 * @param {Response} ctx.response
	 */
	async destroy({ params, request, response }) {
	}
}

module.exports = UpdateDbController

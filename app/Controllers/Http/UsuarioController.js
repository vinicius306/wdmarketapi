'use strict'
const { ReturnApi } = require('../../../lib/ReturnApi')
const Usuario = use("App/Models/Usuario")
/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with usuarios
 */
class UsuarioController {
	/**
	 * Show a list of all usuarios.
	 * GET usuarios
	 *
	 * @param {object} ctx
	 * @param {Request} ctx.request
	 * @param {Response} ctx.response
	 * @param {View} ctx.view
	 */
	async index({ request, response, view }) {
		const user = Usuario.all();
		return user;
	}

	/**
	 * Render a form to be used for creating a new usuario.
	 * GET usuarios/create
	 *
	 * @param {object} ctx
	 * @param {Request} ctx.request
	 * @param {Response} ctx.response
	 * @param {View} ctx.view
	 */
	async create({ request, response, view }) {

	}

	/**
	 * Create/save a new usuario.
	 * POST usuarios
	 *
	 * @param {object} ctx
	 * @param {Request} ctx.request
	 * @param {Response} ctx.response
	 */
	async store({ request, response }) {
		const data = request.post();

		const user = await Usuario.create(data);
		return data;
	}

	/**
	 * Display a single usuario.
	 * GET usuarios/:id
	 *
	 * @param {object} ctx
	 * @param {Request} ctx.request
	 * @param {Response} ctx.response
	 * @param {View} ctx.view
	 */
	async show({ params, request, response, view }) {
	}

	/**
	 * Render a form to update an existing usuario.
	 * GET usuarios/:id/edit
	 *
	 * @param {object} ctx
	 * @param {Request} ctx.request
	 * @param {Response} ctx.response
	 * @param {View} ctx.view
	 */
	async edit({ params, request, response, view }) {
	}

	/**
	 * Update usuario details.
	 * PUT or PATCH usuarios/:id
	 *
	 * @param {object} ctx
	 * @param {Request} ctx.request
	 * @param {Response} ctx.response
	 */
	async update({ params, request, response }) {
	}

	/**
	 * Delete a usuario with id.
	 * DELETE usuarios/:id
	 *
	 * @param {object} ctx
	 * @param {Request} ctx.request
	 * @param {Response} ctx.response
	 */
	async destroy({ params, request, response }) {
	}

	async login({ request, response }) {
		const data = request.all();
		const res = await Usuario.query().where('username', [data.username]).where('senha', [data.password]).fetch();
		console.log(res['rows']);

		if (Array.isArray(res['rows']) && res['rows'].length == 0) {
			return ReturnApi("User ou senha incorreto");
		}
		return ReturnApi(res['rows'], true);
	}

}

module.exports = UsuarioController

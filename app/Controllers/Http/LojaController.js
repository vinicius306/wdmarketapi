'use strict'
const { ReturnApi } = require('../../../lib/ReturnApi')
/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */
const Loja = use('App/Models/Loja')
/**
 * Resourceful controller for interacting with lojas
 */
class LojaController {
	/**
	 * Show a list of all lojas.
	 * GET lojas
	 *
	 * @param {object} ctx
	 * @param {Request} ctx.request
	 * @param {Response} ctx.response
	 * @param {View} ctx.view
	 */
	async index({ request, response, params }) {
		return await Loja.all()
	}

	/**
	 * Render a form to be used for creating a new loja.
	 * GET lojas/create
	 *
	 * @param {object} ctx
	 * @param {Request} ctx.request
	 * @param {Response} ctx.response
	 * @param {View} ctx.view
	 */
	async create({ request, response, view }) {
	}

	/**
	 * Create/save a new loja.
	 * POST lojas
	 *
	 * @param {object} ctx
	 * @param {Request} ctx.request
	 * @param {Response} ctx.response
	 */
	async store({ request, response }) {
	}

	/**
	 * Display a single loja.
	 * GET lojas/:id
	 *
	 * @param {object} ctx
	 * @param {Request} ctx.request
	 * @param {Response} ctx.response
	 * @param {View} ctx.view
	 */
	async show({ params, request, response, view }) {
	}

	/**
	 * Render a form to update an existing loja.
	 * GET lojas/:id/edit
	 *
	 * @param {object} ctx
	 * @param {Request} ctx.request
	 * @param {Response} ctx.response
	 * @param {View} ctx.view
	 */
	async edit({ params, request, response, view }) {
	}

	/**
	 * Update loja details.
	 * PUT or PATCH lojas/:id
	 *
	 * @param {object} ctx
	 * @param {Request} ctx.request
	 * @param {Response} ctx.response
	 */
	async update({ params, request, response }) {
	}

	/**
	 * Delete a loja with id.
	 * DELETE lojas/:id
	 *
	 * @param {object} ctx
	 * @param {Request} ctx.request
	 * @param {Response} ctx.response
	 */
	async destroy({ params, request, response }) {
	}


	async confirm({ request, response }) {
		const { codigo } = request.only(['codigo'])
		const loja = await Loja.query()
			.where('codigo', codigo)
			.where('cod_status', '1')
			.first()
		console.log(loja)
		if (loja !== null) {
			loja.merge({ 'cod_status': 2 })
			await loja.save();
			return ReturnApi('', true)
		}
		return ReturnApi('Loja esta loja nao foi encontrada ou ja esta instalada')
	}
}

module.exports = LojaController

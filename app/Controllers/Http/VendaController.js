'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */
const Venda = use("App/Models/Venda");
/**
 * Resourceful controller for interacting with vendas
 */
class VendaController {
	/**
	 * Show a list of all vendas.
	 * GET vendas
	 *
	 * @param {object} ctx
	 * @param {Request} ctx.request
	 * @param {Response} ctx.response
	 * @param {View} ctx.view
	 */
	async index({ request, response, view }) {
		const venda = await Venda
			.query()
			.with('vendaproduto')
			//await venda.produtos().fetch()
			.fetch();
		return venda
	}

	/**
	 * Render a form to be used for creating a new venda.
	 * GET vendas/create
	 *
	 * @param {object} ctx
	 * @param {Request} ctx.request
	 * @param {Response} ctx.response
	 * @param {View} ctx.view
	 */
	async create({ request, response, view }) {


	}

	/**
	 * Create/save a new venda.
	 * POST vendas
	 *
	 * @param {object} ctx
	 * @param {Request} ctx.request
	 * @param {Response} ctx.response
	 */
	async store({ request, response }) {
		const data = request.except(['cod_venda', 'created', 'modifed', 'venda_produto']);
		const { venda_produto } = request.only(['venda_produto'])

		if (Array.isArray(venda_produto) && venda_produto.length == 0) {
			return response.json({ "success": "false", "Message": "Venda nao possui produtos associados" })
		};

		data.cod_cupom = 0;
		const venda = await Venda.create(data);
		await venda.vendaproduto().createMany(venda_produto);
		return venda
	}

	/**
	 * Display a single venda.
	 * GET vendas/:id
	 *
	 * @param {object} ctx
	 * @param {Request} ctx.request
	 * @param {Response} ctx.response
	 * @param {View} ctx.view
	 */
	async show({ params, request, response, view }) {

	}

	/**
	 * Render a form to update an existing venda.
	 * GET vendas/:id/edit
	 *
	 * @param {object} ctx
	 * @param {Request} ctx.request
	 * @param {Response} ctx.response
	 * @param {View} ctx.view
	 */
	async edit({ params, request, response, view }) {
	}

	/**
	 * Update venda details.
	 * PUT or PATCH vendas/:id
	 *
	 * @param {object} ctx
	 * @param {Request} ctx.request
	 * @param {Response} ctx.response
	 */
	async update({ params, request, response }) {
	}

	/**
	 * Delete a venda with id.
	 * DELETE vendas/:id
	 *
	 * @param {object} ctx
	 * @param {Request} ctx.request
	 * @param {Response} ctx.response
	 */
	async destroy({ params, request, response }) {
	}
}

module.exports = VendaController

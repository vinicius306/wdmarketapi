'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */
const LojaProduto = use('App/Models/LojaProduto')
const { ReturnApi } = require('../../../lib/ReturnApi')
/**
 * Resourceful controller for interacting with lojaprodutos
 */
class LojaProdutoController {
	/**
	 * Show a list of all lojaprodutos.
	 * GET lojaprodutos
	 *
	 * @param {object} ctx
	 * @param {Request} ctx.request
	 * @param {Response} ctx.response
	 * @param {View} ctx.view
	 */
	async index({ request, response, view }) {
		const lojaprodutos = await LojaProduto.all()
		return ReturnApi(lojaprodutos, true)
	}

	/**
	 * Render a form to be used for creating a new lojaproduto.
	 * GET lojaprodutos/create
	 *
	 * @param {object} ctx
	 * @param {Request} ctx.request
	 * @param {Response} ctx.response
	 * @param {View} ctx.view
	 */
	async create({ request, response, view }) {
	}

	/**
	 * Create/save a new lojaproduto.
	 * POST lojaprodutos
	 *
	 * @param {object} ctx
	 * @param {Request} ctx.request
	 * @param {Response} ctx.response
	 */
	async store({ request, response }) {
	}

	/**
	 * Display a single lojaproduto.
	 * GET lojaprodutos/:id
	 *
	 * @param {object} ctx
	 * @param {Request} ctx.request
	 * @param {Response} ctx.response
	 * @param {View} ctx.view
	 */
	async show({ params, request, response, view }) {
	}

	/**
	 * Render a form to update an existing lojaproduto.
	 * GET lojaprodutos/:id/edit
	 *
	 * @param {object} ctx
	 * @param {Request} ctx.request
	 * @param {Response} ctx.response
	 * @param {View} ctx.view
	 */
	async edit({ params, request, response, view }) {
	}

	/**
	 * Update lojaproduto details.
	 * PUT or PATCH lojaprodutos/:id
	 *
	 * @param {object} ctx
	 * @param {Request} ctx.request
	 * @param {Response} ctx.response
	 */
	async update({ params, request, response }) {
	}

	/**
	 * Delete a lojaproduto with id.
	 * DELETE lojaprodutos/:id
	 *
	 * @param {object} ctx
	 * @param {Request} ctx.request
	 * @param {Response} ctx.response
	 */
	async destroy({ params, request, response }) {
	}
}

module.exports = LojaProdutoController

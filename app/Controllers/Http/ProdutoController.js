'use strict'
const axios = require('axios');
const { ReturnApi } = require('../../../lib/ReturnApi')
/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */
const Produto = use("App/Models/Produto")
const Categoria = use("App/Models/Categoria")
const ProdutoCategoria = use("App/Models/ProdutoCategoria")
const Database = use('Database')
/**
 * Resourceful controller for interacting with produtos
 */
class ProdutoController {
	/**
	 * Show a list of all produtos.
	 * GET produtos
	 *
	 * @param {object} ctx
	 * @param {Request} ctx.request
	 * @param {Response} ctx.response
	 * @param {View} ctx.view
	 */
	async index({ request, response, params }) {
		//const Produtos = await Produto.all()//query().with('categorias').fetch()
		const Produtos = await Produto
			.query()
			.whereHas('lojas', (builder) => {
				builder.where('loja.codigo', Database.raw(`?`, [`${params.id}`]))

			})
			//.with('categorias')
			// .innerJoin('loja_produto', function () {
			// 	this
			// 		.on('produto.cod_produto', 'loja_produto.cod_produto')
			// 		.andOn('loja_produto.cod_loja',)
			// })
			.fetch()

		if (Produtos == null) {
			return ReturnApi('Nao foi possivel achar produtos para essa loja')
		}
		const res = await Promise.all(Produtos['rows'].map(p => { return (toBase64(p)) }))

		return ReturnApi(res, true)

	}



	/**
	 * Create/save a new produto.
	 * POST produtos
	 *
	 * @param {object} ctx
	 * @param {Request} ctx.request
	 * @param {Response} ctx.response
	 */
	async store({ request, response }) {
	}

	/**
	 * Display a single produto.
	 * GET produtos/:id
	 *
	 * @param {object} ctx
	 * @param {Request} ctx.request
	 * @param {Response} ctx.response
	 * @param {View} ctx.view
	 */
	async show({ params, request, response, view }) {
	}

	/**
	 * Render a form to update an existing produto.
	 * GET produtos/:id/edit
	 *
	 * @param {object} ctx
	 * @param {Request} ctx.request
	 * @param {Response} ctx.response
	 * @param {View} ctx.view
	 */
	async edit({ params, request, response, view }) {
	}

	/**
	 * Update produto details.
	 * PUT or PATCH produtos/:id
	 *
	 * @param {object} ctx
	 * @param {Request} ctx.request
	 * @param {Response} ctx.response
	 */
	async update({ params, request, response }) {
	}

	/**
	 * Delete a produto with id.
	 * DELETE produtos/:id
	 *
	 * @param {object} ctx
	 * @param {Request} ctx.request
	 * @param {Response} ctx.response
	 */
	async destroy({ params, request, response }) {
	}
}
const toBase64 = async (Produto) => {
	try {
		if (Produto.url_imagem !== null) {
			const res = await axios.get(`http://market.wdvending.com.br/img/produtos/produto_${Produto.cod_produto}/${Produto.url_imagem}`, { responseType: 'arraybuffer' });

			Produto.url_imagem = `data:${res.headers['content-type']};base64,${new Buffer.from(res.data).toString('base64')}`;
		}
		return Produto
	} catch (e) {
		Produto.url_imagem = null
		return Produto
	}
}
module.exports = ProdutoController

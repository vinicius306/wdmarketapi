const ReturnApi = (payload, success = false) => {
	if (!success) {
		return { 'success': success, 'message': payload }
	} else {
		return { 'success': success, 'data': payload }
	}


}

exports.ReturnApi = ReturnApi;
